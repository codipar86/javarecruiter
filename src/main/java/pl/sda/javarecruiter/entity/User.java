package pl.sda.javarecruiter.entity;


import lombok.*;
import org.hibernate.validator.constraints.Length;
import pl.sda.javarecruiter.type.Role;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

@Entity
@Table(name = "users1")
public class User {

    @Setter
    @Getter
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String surname;
    private String pesel;
    @Enumerated(EnumType.STRING)
    private Role role;
    @Email(message = "*Wprowadź poprawny adres email")
    @NotEmpty(message = "*To pole nie może być puste, wprowadź swój adres email")
    private String email;
    @Length(min = 5 , message = "*Hasło musi mieć co najmniej 5 znaków")
    @NotEmpty(message = "*Hasło nie może być puste, podaj hasło")
    private String password;
}

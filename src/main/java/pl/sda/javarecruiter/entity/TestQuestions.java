package pl.sda.javarecruiter.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

@Entity
@Table(name = "test_questions")
public class TestQuestions {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;
    @ManyToOne
    @JoinColumn(name = "test_id")
    private Test test;

    @ManyToMany
    @JoinTable(name = "test_answers",joinColumns = @JoinColumn(name = "test_questions_id"),inverseJoinColumns = @JoinColumn(name = "answer_id"))
    private Set<Answer> answers;

}

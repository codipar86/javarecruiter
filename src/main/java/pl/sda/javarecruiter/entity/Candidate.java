package pl.sda.javarecruiter.entity;

import lombok.*;
import pl.sda.javarecruiter.type.Status;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "candidates")
public class Candidate {

    @Id
    @GeneratedValue
    @Column(name = "candidate_id")
    private Long candidateId;
    @Column(name = "test_result")
    private int testResult;
    @Enumerated(EnumType.STRING)
    @Column(name = "recruitment_status")
    private Status recruitmentStatus;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "candidate_skills",joinColumns = @JoinColumn(name = "candidate_id"),inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skills> skills = new HashSet<>();


}

package pl.sda.javarecruiter.entity;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

@Entity
@Table(name = "answers")
public class Answer {

    @Id
    @GeneratedValue
    private Long answerId;
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;
    @Column(nullable = false, name = "answer_text")
    private String answerText;
    @Column(nullable=false)
    private boolean correction;

}

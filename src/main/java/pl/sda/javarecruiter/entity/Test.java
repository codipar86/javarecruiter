package pl.sda.javarecruiter.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

@Entity
@Table(name = "tests")
public class Test {

    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;
    @OneToMany(mappedBy = "test")
    private Set<TestQuestions> testQuestions;

}

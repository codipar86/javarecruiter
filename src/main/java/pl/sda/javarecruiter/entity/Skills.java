package pl.sda.javarecruiter.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "skills")
public class Skills {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false,name = "java_skills")
    private boolean javaSkill;
    @Column(nullable = false,name = "spring_skills")
    private boolean springSkill;
    @Column(nullable = false,name = "hibernate_skills")
    private boolean hibernateSkill;
    @Column(nullable = false,name = "sql_skills")
    private boolean sqlSkill;

    @ManyToMany
    @JoinTable(name = "candidate_skills",joinColumns = @JoinColumn(name = "skill_id"),inverseJoinColumns = @JoinColumn(name = "candidate_id"))
    private Set<Candidate> candidates = new HashSet<>();

}

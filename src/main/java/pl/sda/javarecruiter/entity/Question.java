package pl.sda.javarecruiter.entity;

import lombok.*;
import pl.sda.javarecruiter.type.DifficultyLevel;
import pl.sda.javarecruiter.type.Modul;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "questions")

public class Question {

    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "question")
    private String ques;
    @Enumerated(EnumType.STRING)
    @Column(name = "difficulty_level")
    private DifficultyLevel difficultyLevel;
    @Enumerated(EnumType.STRING)
    private Modul modul;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "question", orphanRemoval = true)
    private List<Answer> answers;
}

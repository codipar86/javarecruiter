package pl.sda.javarecruiter.mapper;

import pl.sda.javarecruiter.dto.CandidateDto;
import pl.sda.javarecruiter.entity.Candidate;

public class CandidateMapper {

    public static Candidate toEntity(CandidateDto dto) {
        return Candidate.builder()
                .candidateId(dto.getCandidateId())
                .testResult(dto.getTestResult())
                .recruitmentStatus(dto.getRecruitmentStatus())
                .user(dto.getUser())
                .skills(dto.getSkills())
                .build();
    }

    public static CandidateDto toDto(Candidate candidate) {
        return CandidateDto.builder()
                .candidateId(candidate.getCandidateId())
                .testResult(candidate.getTestResult())
                .recruitmentStatus(candidate.getRecruitmentStatus())
                .user(candidate.getUser())
                .skills(candidate.getSkills())
                .build();
    }
}

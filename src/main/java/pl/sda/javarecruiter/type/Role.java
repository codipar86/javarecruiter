package pl.sda.javarecruiter.type;

public enum Role {
    ADMIN,
    CANDIDATE,
    HR
}

package pl.sda.javarecruiter.type;

public enum Modul {
    JAVA,
    SPRING,
    HIBERNATE,
    SQL
}

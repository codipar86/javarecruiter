package pl.sda.javarecruiter.type;

public enum DifficultyLevel {
    LOW,
    MEDIUM,
    HIGH
}

package pl.sda.javarecruiter.type;

public enum Status {
    BEFORE,
    READY,
    IN_PROGRESS,
    IN_VERYFICATION,
    ACCEPTED,
    REJECTED;
}

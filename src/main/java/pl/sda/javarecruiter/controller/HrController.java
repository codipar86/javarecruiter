package pl.sda.javarecruiter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.javarecruiter.service.CandidateService;

@Controller
@RequestMapping("/hr")
public class HrController {

    @Autowired
    private CandidateService candidateService;

    @GetMapping("/")
    public String home() {
        return "hr.jsp";
    }


    @GetMapping("/allcandidate")
    public ModelAndView showAllCandidateTable() {
        ModelAndView mav = new ModelAndView("allcandidate.jsp");
        mav.addObject("candidates", candidateService.findAllCandidate());
        return mav;
    }
}

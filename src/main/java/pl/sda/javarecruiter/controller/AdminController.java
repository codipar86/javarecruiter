package pl.sda.javarecruiter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.javarecruiter.entity.Question;
import pl.sda.javarecruiter.service.QuestionService;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final QuestionService questionService;

    @Autowired
    public AdminController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/")
    public String home() {
        return "admin.jsp";
    }

    @PostMapping(value = "/addquestion")
    public String addQuestion(@ModelAttribute("question") Question question) {
        questionService.saveQuestion(question);
        return "redirect:/admin/addquestion";
    }

    @GetMapping("/addquestion")
    public String addQuestion(Model model) {
        model.addAttribute("question", new Question());
        return "addquestion.jsp";
    }
}

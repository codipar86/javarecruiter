package pl.sda.javarecruiter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.javarecruiter.entity.User;
import pl.sda.javarecruiter.service.UserServiceImpl;

@Controller
public class UserController {

    private final UserServiceImpl userServiceImpl;


    @Autowired
    public UserController(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @GetMapping(value = "/users")
    public String usersPage(Model model) {
        model.addAttribute("users", userServiceImpl.findAll());
        return "users.jsp";
    }

    @PostMapping(value = "/adduser")
    public String addUser(@ModelAttribute("user") User user) {
        userServiceImpl.saveUser(user);
        return "redirect:/candidate";
    }

    @GetMapping(value = "/adduser")
    public String addUserPage(Model model) {
        model.addAttribute("user", new User());
        return "adduser.jsp";
    }

}

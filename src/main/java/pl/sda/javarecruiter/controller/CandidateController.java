package pl.sda.javarecruiter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.javarecruiter.entity.Candidate;
import pl.sda.javarecruiter.entity.Skills;
import pl.sda.javarecruiter.service.CandidateService;

@Controller
@RequestMapping("/candidate")
public class CandidateController {

    private final CandidateService candidateService;

    @Autowired
    public CandidateController(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @GetMapping("/")
    public String home() {
    return "candidate.jsp";
}

    @GetMapping("/addcandidate")
    public String addCandidatePage(Model model){
        model.addAttribute("candidate", new Candidate());
        model.addAttribute("skills", new Skills());
        return "addcandidate.jsp";
    }

    @PostMapping("/addcandidate")
    public String addCandidate(@ModelAttribute ("candidate") Candidate candidate, @ModelAttribute("skills") Skills skills) {
        candidateService.saveCandidate(candidate, skills);
        return "redirect:/candidate/";
    }
}

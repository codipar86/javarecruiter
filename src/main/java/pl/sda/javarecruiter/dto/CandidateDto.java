package pl.sda.javarecruiter.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.javarecruiter.entity.Skills;
import pl.sda.javarecruiter.entity.User;
import pl.sda.javarecruiter.type.Status;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CandidateDto {

    private Long candidateId;
    private int testResult;
    private Status recruitmentStatus;
    private User user;
    private Set<Skills> skills;
}

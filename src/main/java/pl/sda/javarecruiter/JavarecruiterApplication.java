package pl.sda.javarecruiter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
public class JavarecruiterApplication {

	public static void main(String[] args) {

		SpringApplication.run(JavarecruiterApplication.class, args);

	}
}

package pl.sda.javarecruiter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.javarecruiter.entity.Answer;

import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {

    List<Answer> findByCorrection(boolean correction);

//    List<Answer> query(SqlSpecification sqlSpecification);

    void deleteByAnswerId(Long id);

}

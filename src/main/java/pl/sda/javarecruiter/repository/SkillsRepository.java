package pl.sda.javarecruiter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.javarecruiter.entity.Candidate;
import pl.sda.javarecruiter.entity.Skills;

import java.util.List;

@Repository
public interface SkillsRepository extends JpaRepository<Skills, Long> {

    List<Skills> findByCandidates(Candidate candidate);

//    List<Skills> query(SqlSpecification sqlSpecification);

    Skills deleteAllById(Long id);

}

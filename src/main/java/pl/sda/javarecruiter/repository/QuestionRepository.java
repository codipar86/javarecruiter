package pl.sda.javarecruiter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.javarecruiter.entity.Question;
import pl.sda.javarecruiter.type.DifficultyLevel;
import pl.sda.javarecruiter.type.Modul;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findByModul(Modul modul);

    List<Question> findByDifficultyLevel(DifficultyLevel difficultyLevel);

//    List<Question> query(SqlSpecification sqlSpecification);

    void deleteById(Long id);


}

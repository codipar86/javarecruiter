package pl.sda.javarecruiter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.sda.javarecruiter.entity.Candidate;
import pl.sda.javarecruiter.entity.Skills;

import java.util.List;

@Repository
public interface CandidateRepository extends JpaRepository <Candidate,Long> {

    List<Candidate> findBySkills(Skills skills);

    List<Candidate> findByTestResultIsGreaterThanEqual(int minimumTestResult);

//    List<Candidate> query(SqlSpecification sqlSpecification);

    void deleteById(Long id);

//    @Query("select c from Candidate")
//    List<Candidate> findAllCandidate();
}

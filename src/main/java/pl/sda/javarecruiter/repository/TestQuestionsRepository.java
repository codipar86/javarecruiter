package pl.sda.javarecruiter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.javarecruiter.entity.TestQuestions;

@Repository
public interface TestQuestionsRepository extends JpaRepository<TestQuestions, Long> {

//    List<TestQuestions> query(SqlSpecification sqlSpecification);

    void deleteById(Long id);
}

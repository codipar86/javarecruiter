package pl.sda.javarecruiter.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

@Repository
public interface SqlSpecification extends Specification{

    String toSqlQuery();
}

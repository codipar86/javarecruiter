package pl.sda.javarecruiter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.javarecruiter.entity.Test;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {

//    List<Test> query(SqlSpecification sqlSpecification);

    void deleteById(Long id);

}

package pl.sda.javarecruiter.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.javarecruiter.entity.User;
import pl.sda.javarecruiter.type.Role;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Override
    boolean existsById(Long id);

    List<User> findAllByRole(Role role);

    User findByNameAndSurname(String name, String surname);

    User findByPesel(String pesel);

//    List<User> query(SqlSpecification sqlSpecification);

    void deleteByEmail(String email);

    @Override
    void deleteById(Long aLong);

    User findByEmail(String email);

    User findByRole(String role);

}

package pl.sda.javarecruiter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.javarecruiter.dto.CandidateDto;
import pl.sda.javarecruiter.entity.Candidate;
import pl.sda.javarecruiter.entity.Skills;
import pl.sda.javarecruiter.entity.User;
import pl.sda.javarecruiter.mapper.CandidateMapper;
import pl.sda.javarecruiter.repository.CandidateRepository;
import pl.sda.javarecruiter.repository.SkillsRepository;
import pl.sda.javarecruiter.repository.UserRepository;
import pl.sda.javarecruiter.type.Role;
import pl.sda.javarecruiter.type.Status;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class CandidateService {

//    @Autowired
//    private UserRepository userRepository;

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private SkillsRepository skillsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

//    @Override
//    public User findUserByEmail(String email) throws UsernameNotFoundException{
//        User user = userRepository.findByEmail(email);
//        return user;
//    }

//    @Override
//    public void saveUser(User user) {
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
//        user.setRole(Role.CANDIDATE);
//        userRepository.save(user);
//    }

    public void saveCandidate(Candidate candidate, Skills skills) {
        candidate.setRecruitmentStatus(Status.READY);
        candidate.setTestResult(0); // przemyśleć jak to zrobić z tymi wynikami testów
        Authentication loggedUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedUser.getName();
        candidate.setUser(userRepository.findByEmail(username));
        candidate.getSkills().add(skills);
//        skills.getCandidates().add(candidate);
        candidateRepository.save(candidate);

    }

    public List<CandidateDto> findAllCandidate() {
        return candidateRepository.findAll()
                .stream()
                .map(entity -> CandidateMapper.toDto(entity))
                .collect(Collectors.toList());
    }

}

package pl.sda.javarecruiter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.javarecruiter.entity.Question;
import pl.sda.javarecruiter.repository.QuestionRepository;

@Service
public class QuestionService {

    @Autowired
    public QuestionRepository questionRepository;

//    public QuestionService(QuestionRepository questionRepository) {
//        this.questionRepository = questionRepository;
//    }

    public void saveQuestion(Question question) {
        for (int i = 0 ; i < question.getAnswers().size() ; i++) {
            question.getAnswers().get(i).setQuestion(question);
        }
        questionRepository.save(question);
    }
}

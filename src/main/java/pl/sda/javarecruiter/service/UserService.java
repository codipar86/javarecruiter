package pl.sda.javarecruiter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.javarecruiter.entity.User;
import pl.sda.javarecruiter.repository.UserRepository;

public interface UserService {

public User findUserByEmail(String emial);
public void saveUser(User user);

}

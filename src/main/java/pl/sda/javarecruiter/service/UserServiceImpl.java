package pl.sda.javarecruiter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.javarecruiter.entity.User;
import pl.sda.javarecruiter.repository.UserRepository;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    public UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void resetPassword(Long id, String newPassword) {
        User one = userRepository.getOne(id);
        one.setPassword(newPassword);
    }

    @Override
    public User findUserByEmail(String emial) {
        return null;
    }

    @Override
    public void saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public List findAll() {
        return userRepository.findAll();
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public void deleteUser(String email) {
        userRepository.findByEmail(email);
    }

    //   void addUser()
}


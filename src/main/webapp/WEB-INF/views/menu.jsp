<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<ul>
    <li><a href="${pageContext.servletContext.contextPath}/">Home</a></li>

    <sec:authorize access="hasAuthority('CANDIDATE')">
        <li><a href="${pageContext.servletContext.contextPath}/candidate/addcandidate">Dodaj umiejętności</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/mytests">Moje testy</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/mytestresults">Wyniki z testów</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/mystatus">Status mojej kandydatury</a></li>
    </sec:authorize>

    <sec:authorize access="hasAuthority('ADMIN')">
        <li><a href="${pageContext.servletContext.contextPath}/admin/addquestion">Dodaj pytanie</a></li>

    </sec:authorize>

    <sec:authorize access="hasAuthority('HR')">
        <li><a href="${pageContext.servletContext.contextPath}/hr/allcandidate">Wszyscy kandydaci</a></li>

    </sec:authorize>


    <sec:authorize access="isAuthenticated()">
        <li><a href="<c:url value="/logout" />">Logout</a></li>
    </sec:authorize>
</ul>
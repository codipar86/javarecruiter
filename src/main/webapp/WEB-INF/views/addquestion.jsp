<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Dodaj pytanie</title>
    <link href="${pageContext.servletContext.contextPath}/resources/css/app.css" rel="stylesheet">
</head>
<body>

<jsp:include page="menu.jsp" />

<br>

<form:form action="addquestion" modelAttribute="question" method="post">
    Pytanie : <form:input type="textarea" path="ques"/> <br/>
    Poziom doświadczenia : <form:select path="difficultyLevel">
           <form:option value="LOW">JUNIOR</form:option>
           <form:option value="MEDIUM">REGULAR</form:option>
           <form:option value="HIGH">SENIOR</form:option>
</form:select><br/>
    Moduł : <form:select path="modul">
    <form:option value="JAVA">JAVA</form:option>
    <form:option value="SPRING">SPRING</form:option>
    <form:option value="HIBERNATE">HIBERNATE</form:option>
    <form:option value="SQL">SQL</form:option>
</form:select><br/>
    Odpowiedź A :
    <form:input type="textarea" path="answers[0].answerText"/>
    <form:checkbox path="answers[0].correction"/> <br>
    Odpowiedź B :
    <form:input type="textarea" path="answers[1].answerText"/>
    <form:checkbox path="answers[1].correction"/> <br>
    Odpowiedź C :
    <form:input type="textarea" path="answers[2].answerText"/>
    <form:checkbox path="answers[2].correction"/> <br>
    Odpowiedź D :
    <form:input type="textarea" path="answers[3].answerText"/>
    <form:checkbox path="answers[3].correction"/> <br>
    <input type="submit" value="Dodaj"/>
</form:form>

</body>
</html>

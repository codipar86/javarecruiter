
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Wszyscy kandydaci</title>
    <link href="${pageContext.servletContext.contextPath}/resources/css/app.css" rel="stylesheet">
</head>
<body>

<jsp:include page="menu.jsp" />

Lista wszystkich kandydatów:
<br>
<table>

    <thead>
    <tr>
        <td>Imię</td>
        <td>Nazwisko</td>
        <td>Email</td>
        <td>Status</td>

    </tr>
    </thead>
    <tbody>

    <c:forEach items="${candidates}" var="candidate">
        <tr>
            <td>${candidate.user.name}</td>
            <td>${candidate.user.surname}</td>
            <td>${candidate.user.email}</td>
            <td>${candidate.recruitmentStatus}</td>

        </tr>
    </c:forEach>

    </tbody>

</table>

</body>
</html>

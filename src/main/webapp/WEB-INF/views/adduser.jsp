<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%--
  Created by IntelliJ IDEA.
  User: kamil
  Date: 21.07.2018
  Time: 14:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add User</title>
    <link href="${pageContext.servletContext.contextPath}/resources/css/app.css" rel="stylesheet">
</head>
<body>
<jsp:include page="menu.jsp" />

<form:form action="adduser" modelAttribute="user"
           method="post">
    Imię : <form:input type="text" path="name"/> <br/>
    Nazwisko : <form:input type="text" path="surname"/> <br/>
    PESEL : <form:input type="text" path="pesel"/> <br/>

    Rola : <form:select path="role">
 <sec:authorize access="hasAuthority('ADMIN')">
    <form:option value="ADMIN">ADMIN</form:option>
    <form:option value="HR">HR</form:option>
    </sec:authorize>
    <form:option value="CANDIDATE">CANDIDATE</form:option>
</form:select><br/>

    Email : <form:input type="text" path="email"/> <br/>
    Password : <form:input type="password" path="password"/> <br/>

    <input type="submit" value="Dodaj"/>
</form:form>

</body>
</html>

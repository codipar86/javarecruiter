<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Add Candidate</title>
    <link href="${pageContext.servletContext.contextPath}/resources/css/app.css" rel="stylesheet">
</head>
<body>

<jsp:include page="menu.jsp" />

Dodaj swoje umiejętności:

<form:form action="addcandidate" modelAttribute="skills" method="post">
    Java: <form:checkbox path="javaSkill"/> <br>
    Spring: <form:checkbox path="springSkill"/> <br>
    Hibernate: <form:checkbox path="hibernateSkill"/> <br>
    SQL: <form:checkbox path="sqlSkill"/> <br>
    <input type="submit" value="Poproś o testy"/>
</form:form>

</body>
</html>

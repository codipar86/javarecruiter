<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Rekrutacja</title>
    <link href="${pageContext.servletContext.contextPath}/resources/css/app.css" rel="stylesheet"></head>
<body>

<jsp:include page="menu.jsp" />

<h1>Aplikacja do rekrutacji</h1> <br>

<sec:authorize access="isAnonymous()">
<a href="/login"><button>zaloguj</button></a> <br>
</sec:authorize>

<a href="/adduser"><button>zarejestruj</button></a>

</body>
</html>